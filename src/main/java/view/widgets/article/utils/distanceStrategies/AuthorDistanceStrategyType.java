package view.widgets.article.utils.distanceStrategies;

public enum AuthorDistanceStrategyType {
	SemanticDistance, SemanticPrunnedByCoCitOrCoAuth, CoAuthorshipDistance, CoCitationsDistance
}