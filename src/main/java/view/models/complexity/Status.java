package view.models.complexity;

public enum Status {
	SELECTED, DESELECTED, INDETERMINATE
}
