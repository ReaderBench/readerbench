package services.comprehensionModel.utils.indexer.graphStruct;

public enum CiNodeType {
	Semantic,
	Syntactic,
	Active,
	Inactive
}