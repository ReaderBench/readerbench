package services.comprehensionModel.utils.indexer.graphStruct;

public enum CiEdgeType {
	Semantic,
	Syntactic
}