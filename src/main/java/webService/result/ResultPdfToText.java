package webService.result;

public class ResultPdfToText {

	private String content;

	public ResultPdfToText(String content) {
		super();
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
