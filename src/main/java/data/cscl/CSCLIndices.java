package data.cscl;

import java.util.ResourceBundle;

public enum CSCLIndices {
	NO_CONTRIBUTION(true),
	OVERALL_SCORE(true),
	PERSONAL_KB(false),
	SOCIAL_KB(true),
	INTER_ANIMATION_DEGREE(false),
	INDEGREE(true),
	OUTDEGREE(true),
	BETWEENNESS(true),
	CLOSENESS(true),
	ECCENTRICITY(true),
	RELEVANCE_TOP10_TOPICS(true),
	NO_NOUNS(false),
	NO_VERBS(false),
	NO_NEW_THREADS(false),
	AVERAGE_LENGTH_NEW_THREADS(false),
	NEW_THREADS_OVERALL_SCORE(false),
	NEW_THREADS_INTER_ANIMATION_DEGREE(false),
	NEW_THREADS_CUMULATIVE_SOCIAL_KB(false);

	private boolean isUsedForTimeModeling;

	private CSCLIndices(boolean isUsedForTimeModeling) {
		this.isUsedForTimeModeling = isUsedForTimeModeling;
	}

	public boolean isUsedForTimeModeling() {
		return isUsedForTimeModeling;
	}

	public String getDescription() {
		return ResourceBundle.getBundle("utils.localization.CSCL_indices_descr").getString(this.name());
	}

	public String getAcronym() {
		return ResourceBundle.getBundle("utils.localization.CSCL_indices_acro").getString(this.name());
	}
}
